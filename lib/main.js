/*
 * Copyright 2012 mozbugbox <mozbugbox@yahoo.com.au>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// The main module of the come2china Add-on.
// Inspired by/copied: https://github.com/yiqiao/Unblock-Youku

console.debug = function () {;} // Turn off debug

var data = require("sdk/self").data;
var header = require("./header");
var events = require("sdk/system/events")
var xulApp = require("sdk/system/xul-app");
var utils = require("sdk/window/utils");
var { Proxy, Proxy2 }= require("./proxy");

function Control() {
    var this_control = this;
    this.status_icon = null;
    this.menu_id = null;

    var widget_info = {
        // Mandatory string used to identify your widget in order to
        // save its location when the user moves it in the browser.
        // This string has to be unique and must not be changed over time.
        id: "come2china-main-icon",

        // A required string description of the widget used for
        // accessibility, title bars, and error reporting.
        label: "Come to China!",
        tooltip: "Click and Come to China...",


        // An optional string URL to content to load into the widget.
        // This can be local content or remote content, an image or
        // web content. Widgets must have either the content property
        // or the contentURL property set.
        //
        // If the content is an image, it is automatically scaled to
        // be 16x16 pixels.
        contentURL: data.url("zhong-disable.png"),

        // Add a function to trigger when the Widget is clicked.
        onClick: function(event) {
            console.debug("header.sogou_proxy:", header.sogou_proxy);
            this_control.toggle(header.sogou_proxy.activated);
        },
    };

    if (xulApp.is("Fennec")) {
        // get a global window reference
        var recent = utils.getMostRecentBrowserWindow();
        var nw = recent.NativeWindow;
        this.menu_id = nw.menu.add({
            name: widget_info.label,
            icon: widget_info.contentURL,
            callback: widget_info.onClick,
            checkable: true
        });
        nw.menu.update(this.menu_id, {checked: false,});
 
    } else {
        var Widget = require("sdk/widget").Widget;
        this.status_icon = new Widget(widget_info);
    }
}

Control.prototype = {
    //proxy: new Proxy(),
    proxy: new Proxy2(),

    toggle: function(state) {
        console.debug("toggle state:", state);
        var signal = "http-on-modify-request";
        var icon_data;
        var label;
        header.sogou_proxy.activated = !state;
        if (state == false) {
            // Turn on proxy, set preferences...
            icon_data = data.url("zhong.png");
            label = "Come to China!";

            events.on(signal, header.modify_header);
            this.proxy.install();

        } else {
            icon_data = data.url("zhong-disable.png");
            label = "Click and Come to China...";

            events.off(signal, header.modify_header);
            this.proxy.restore();
        }

        if (this.status_icon !== null) {
            this.status_icon.contentURL = icon_data;
            this.status_icon.tooltip = label;
        }
        if (this.menu_id !== null) {
            var recent = utils.getMostRecentBrowserWindow();
            var nw = recent.NativeWindow;
            nw.menu.update(this.menu_id, {
                icon: icon_data,
                checked: header.sogou_proxy.activated
            });
        }
    },
};

function main() {
    header.init_proxy();

    console.debug("loading main...");
    var control = new Control();
}

exports.main = main;
